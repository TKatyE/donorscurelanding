<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>index</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
<?php


  if(isset($_POST['email'])) {
    $email_to = "admin@donorscure.org";
    $email_subject = "I'm a Researcher and am interested in DonorsCure.org";

    
    $email = $_POST['email'];
		
		$howmuch = "";
		$howmuch_select = $_POST['optionsRadio'];
		
		
		if ($howmuch_select == 'under5000') {
			$howmuch .= "Under $5000\n\r";
		} 
		if ($howmuch_select == 'low') {
			$howmuch .= "$5000-$10,000\n\r";
		} 
		if ($howmuch_select == 'mid') {
			$howmuch .= "$10,000-$25,000\n\r";
		} 
		if ($howmuch_select == "high") {
			$howmuch .= "Over $25,000\n\r";
		}
		
		$howoften = "";
		$howoften_select = $_POST['optionsRadio_often'];
		
		
		if ($howoften_select == 'everymonth') {
			$howoften .= "Would update every month.\n\r";
		} 
		if ($howoften_select == 'everyweek') {
			$howoften .= "Would update every week.\n\r";
		} 
		if ($howoften_select == 'notatall') {
			$howoften .= "Wouldn't update at all.\n\r";
		} 
		if ($howoften_select == "fewmonths") {
			$howoften .= "Would update every few months.\n\r";
		}
		
		
		//echo $howoften;
		
    $email_message = "";
		//echo $email_message;
		
		$email_message .= "Email: ". $email ."\r\n";
		//echo $email_message;
		
		$email_message .= $howmuch . "\r\n";
		$email_message .= "\r\n" . $howoften . "\r\n";
			
		//echo $email_message;
		
    $headers = "From: " . $email . "\r\n" . "Reply-To: admin@donorscure.org" . "\r\n" . "X-Mailer: PHP/" . phpversion();
    
    mail($email_to, $email_subject, $email_message, $headers);
    
    ?>
    	<div class="row">
    		<div class="col-lg-12 col-xs-12">
    			<h3 style="margin-top: 80px;">Thank you for your response and interest!  As we get closer to launch, we'll keep you posted.</h3>
    	
    	<?php
  			} else {
  		?>
			<div class="row">
				<div class="col-lg-12 col-xs-12">
  				<h3 style="margin-top: 80px;">There was a problem with your submission.  Please make sure you include your email address!</h3>
					<h2 class="center"><a href="donor.php">Try Again</a></h2>
   		
   		<?php 
   			}
			?>
    		
      </div>
    </div>
    <div class="navbar navbar-fixed-bottom"></div>

  </body>
</html>

