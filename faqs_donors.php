<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>DonorsCure.org - FAQ's for Donors</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="bootstrap/css/overrides.css" rel="stylesheet" media="screen">
  <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<script src="bootstrap/js/css3-mediaqueries.js"></script>	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <div class="container" style="margin-bottom: 100px;">
    <div class="navbar navbar-fixed-top"></div>
    
    <div class="row">
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
      <div class="col-xs-6 col-lg-6 col-sm-6">
        <a href="index.php"><img src="logo_working.png" class="logo" style="margin-bottom: 80px;"></a>
      </div>
      <div class="col-xs-6 col-lg-3 col-sm-3"></div>
    </div>
  
  <div class="row">
  
  	<div class="col-lg-12 col-12 col-xs-12">
  	
  	<h2>Frequently Asked Questions (for Donors)</h2>
  	
		<h5>How does Donors Cure work?</h5>
		<p>Donors Cure is an online, 501(c)(3) nonprofit organization that aims to fund 
		biomedical research projects through donations from members of the general public.</p>
		<p>Universities and institutions around the United States have signed up to participate 
		in Donors Cure, and researchers at these universities have posted requests for funding 
		so that they can work on specific biomedical research projects.  Their proposals cover 
		anything from short-term projects aimed at testing potential larger ideas to longer-term 
		goals that involve multiple smaller projects and other researchers and anything in between, 
		as long as it's a viable idea that will further biomedical research.</p>
	
		<p>You can browse through the current proposals and choose one or more to donate any 
		amount to.  When the project period closes, and if the project has reached its funding goal, 
		the money is transferred to the researcher's university, and they can begin work on the project.  
		When this happens, and throughout the time the researcher works on their project, 
		you'll be able to see updates about how progress is going.</p>
		
		<h5>What makes Donors Cure different from other science crowdfunding websites?</h5>
		<p>While other crowdfunding platforms exist, Donors Cure focuses specifically on 
		biomedical research that is of the quality to be funded by traditional grant-funding 
		mechanisms.  Unlike these other platforms, Donors Cure requires universities and institutions as a 
		whole to participate.  In order create an account to post 
		a project, the researcher leading the project must be a current researcher 
		at one of these participating institutions.</p> 

		<p>When a researcher does post a project, the project is assessed for how reasonable 
		and feasible it is to complete by the scientific team at Donors Cure.  If the project passes, 
		it is posted on DonorsCure.org with details of the its goals and an estimated breakdown of 
		what the funds are going to be used for.  Researchers are also required to post their 
		academic CV so you can see their background, track record and publications.</p>
		
		<h5>How do I know the researchers are really researchers and projects are viable?</h5>
		<p>While Donors Cure doesn't go so far as to say anyone can come up with a project 
		they want funding for, we do encourage innovative ideas that are slightly 
		outside-of-the-box.  As a result, the proposals posted on Donors Cure cover a 
		broad range of research areas and may be for ideas that are really well developed 
		and have been worked on for years (but still need more work!) or for ideas that 
		show a lot of potential but haven't had all of the kinks worked out yet.  
		Regardless of where the project falls on this spectrum, the researchers who are 
		leading them are the 'real deal' and are already hard-at-work on the next 
		Nobel Prize at a university or institution that is participating in Donors Cure.</p>
		
		<p>Before a project 'goes live' on DonorsCure.org, a longer, more scientific
		 description of the proposal is reviewed internally by scientific staff at Donors Cure to 
		assess first that it meets the requirements for what constitutes 'research' and then that the 
		research is reasonable and scientifically sound and feasible for the budget and timeframe suggested.  Additionally, 
		Donors Cure works with researchers to ensure that the part of their proposal that donors see are written in a way that adequately conveys 
		their project idea but is still understandable to the non-scientist.</p>
		
		<h5>How do I know my donations are truly funding the projects you say they are funding?</h5>
		<p>When you make a donation to a project, the money first goes to Donors Cure.  
		When the project's funding period closes and if the project's funding target has been reached, 
		the total amount is transferred to the research fiscal accounting office at the researcher's institution.  
		The institution is then responsible for releasing funds to the researcher.</p>
		
		<h5>What happens if a project isn't fully funded after its funding period ends?</h5>
		<p>When a researcher submits a project proposal, they can choose an amount of one to six months 
		for the funding period.  If, after this timeframe, the project has not raised enough funds to reach its goal, 
		the researcher can choose to extend the funding period in one month increments until the total length of 
		time that a project is posted on Donors Cure reaches six months.</p>
		
		<p>Donors Cure will not refund donations.  However, when you check out, you'll be able to choose 
		how you want your money to be redirected if the project does not reach its goal from three options: 1) Reallocate the donation to a 
		similar project at the same university/institution, 2) reallocate the donation to a similar project at any university or 3) 
		donate the money directly to Donors Cure to use as they see fit.</p>
		
		<h5>Is my donation tax-deductible?</h5>
		<p>Donors Cure is currently in the process of applying for 501(c)(3) status.  All donations will be retroactively tax-deductible upon approval.</p>
		
		<h5>What do I get from donating?</h5>
		<p>While Donors Cure primarily wants to fund research, we also want to help make science 
		research more accessible to the general public, and we encourage researchers to actively engage with their donors.  
		After a project is funded and research begins, researchers are required to submit semi-frequent updates 
		about the progress of their work and are encouraged and guided to make these easy to understand and reflect the 
		highs and lows of daily lab work.  In addition to these more informal updates, if a paper or other publication 
		comes out of the funded researchers, donors will be notified.</p>
		
		</div>
	</div>
</body>
</html>
